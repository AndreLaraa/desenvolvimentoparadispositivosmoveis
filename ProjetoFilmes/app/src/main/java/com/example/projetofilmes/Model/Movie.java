package com.example.projetofilmes.Model;

import android.graphics.Bitmap;

import java.util.ArrayList;
import java.util.List;

public class Movie {

    public String Title;
    public String Year;
    public String Rated;
    public String Released;
    public String Runtime;
    public String Genre;
    public String Director;
    public String Writer;
    public String Actors;
    public String Plot;
    public String Language;
    public String Country;
    public String Awards;
    public String Poster;
    public String Metascore;
    public String imdbRating;
    public String imdbVotes;
    public String imdbID;
    public String Type;
    public String Response;
    public List<String> Images = null;
    public String totalSeasons;

    public boolean ComingSoon;

    public transient Bitmap _bitmapImage;
    public transient ArrayList<String> genres;
    public Movie() {
        genres = new ArrayList<>();
    }

}
