package com.example.projetofilmes;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.Volley;
import com.example.projetofilmes.Model.DataModel;
import com.example.projetofilmes.Model.Enums.Favorite;
import com.example.projetofilmes.Model.Movie;
import com.example.projetofilmes.Model.Interfaces.ResultHandler;
import com.example.projetofilmes.Model.User;

public class RecyclerMovieItem extends RecyclerView.Adapter<RecyclerMovieItem.CardsViewHolder> {

    public static class CardsViewHolder extends RecyclerView.ViewHolder {
        ImageView imageViewCard;
        View viewContent;
        TextView textViewNameMovie;
        Button favtBtn;
        Movie movie;

        private LoadingDialog loadingDialog;

        CardsViewHolder(View itemView, int middle) {
                super(itemView);

                favtBtn = itemView.findViewById(R.id.favtBtn);
                imageViewCard = itemView.findViewById(R.id.imageViewMovie);
                viewContent = itemView.findViewById(R.id.viewContent);
                textViewNameMovie = itemView.findViewById(R.id.textViewNameMovie);

                imageViewCard.getLayoutParams().width = middle;
                imageViewCard.getLayoutParams().height = 300;
                viewContent.getBackground().setAlpha(150);

                loadingDialog = new LoadingDialog(viewContent.getContext());

                itemView.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        itemClick(v);
                    }
                });

                favtBtn.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        favItemClick(v);
                    }
                });

        }

        private void itemClick(View view) {

            Log.d("AAAAA", "VIEW" + movie.imdbID + "  "+ getLayoutPosition());
            Intent intent = new Intent(view.getContext(), MovieDetailsActivity.class);
            intent.putExtra("imdbId", movie.imdbID);
            view.getContext().startActivity(intent);
        }

        private void favItemClick(View view) {
            loadingDialog.show();

            DataModel.shared().setMovieFavorite(movie, new ResultHandler<Favorite>() {

                @Override
                public void onSuccess(Favorite data) {

                    Context context = itemView.getContext();
                    int id_movie = data == Favorite.on ? R.drawable.fav_on : R.drawable.fav_off;

                    Drawable d = context.getResources().getDrawable(id_movie, context.getTheme());
                    favtBtn.setBackground(d);

                    loadingDialog.hide();

                }

                @Override
                public void onFailure(Exception e) {
                    handlingFailure(e);
                }
            });

        }

        private void handlingFailure(Exception e) {
        }
    }

    @NonNull
    @Override
    public CardsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_movie_item, parent, false);
        int middle = v.getLayoutParams().width / 2;
        return new CardsViewHolder(v, middle);
    }

    @Override
    public void onBindViewHolder(@NonNull final CardsViewHolder holder, final int position) {
        Drawable d;
        Context context = holder.itemView.getContext();
        Movie _movie = DataModel.shared().getMovies().get(position);
        holder.movie = _movie;


//        if (movie._bitmapImage != null)
//            holder.imageViewCard.setImageBitmap(movie._bitmapImage);

//        if(_movie.Poster != null && _movie.Poster.toLowerCase().contains("https")) {
//            RequestQueue queue = Volley.newRequestQueue( holder.itemView.getContext());
//            ImageRequest ir = new ImageRequest(_movie.Poster, new Response.Listener<Bitmap>() {
//                @Override
//                public void onResponse(Bitmap response) {
//                    // callback
////                Singleton.shared().movies.get(0).image = response;
////                recyclerMovies_itemAdapter.notifyDataSetChanged();
//                    holder.imageViewCard.setImageBitmap(response);// imageViewCards.setImageBitmap(response);
//                }
//            }, 500, 500, null, null);
//            queue.add(ir);
//            queue.start();
//        }


        if (_movie.Title != null)
            holder.textViewNameMovie.setText(_movie.Title);

        if (User.shared().containFavorited(_movie.imdbID))
            d = context.getResources().getDrawable(R.drawable.fav_on, context.getTheme());
        else
            d = context.getResources().getDrawable(R.drawable.fav_off, context.getTheme());

        holder.favtBtn.setBackground(d);

    }

    @Override
    public int getItemCount() {
        return DataModel.shared().getMovies().size();
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }
}
