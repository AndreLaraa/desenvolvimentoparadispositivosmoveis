package com.example.projetofilmes.Model;

import android.util.Log;

import com.example.projetofilmes.Model.Interfaces.ResultHandler;
import com.example.projetofilmes.Model.Interfaces.UserMethodsDataBase;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.ArrayList;

public class User implements UserMethodsDataBase {

    // Singleton object and methods
    private FirebaseUser currentUser;
    private static User _shared = new User();

    protected ArrayList<String> favoritedMovieIds = new ArrayList<>();

    public static User shared() {
        _shared.currentUser = FirebaseAuth.getInstance().getCurrentUser();
        return _shared;
    }
    private void User(){ }

    public Boolean isLogged(){ return _shared.currentUser != null; }

    public boolean containFavorited(String imdb) { return _shared.favoritedMovieIds.contains(imdb);}

    @Override
    public void create(String email, String password, ResultHandler<Boolean> handler) {
        DBServices.shared().create(email, password, handler);
    }

    @Override
    public void login(String email, String password, ResultHandler<Boolean> handler) {
        DBServices.shared().login(email,password,handler);
    }

    @Override
    public String getEmail() {
        return DBServices.shared().getEmail();
    }

    @Override //Return the IMDB IDS
    public void getFavorites(final ResultHandler<ArrayList<String>> handler) {
        DBServices.shared().getFavorites(new ResultHandler<ArrayList<String>>() {
            @Override
            public void onSuccess(ArrayList<String> data) {
                _shared.favoritedMovieIds = data;
                handler.onSuccess(data);
            }

            @Override
            public void onFailure(Exception e) { handler.onFailure(e); }
        });
    }

    @Override
    public void addFavorite(final String imdbId, final ResultHandler<ArrayList<String>> handler) {
        DBServices.shared().addFavorite(imdbId, new ResultHandler<ArrayList<String>>() {
            @Override
            public void onSuccess(ArrayList<String> data) {
                if (_shared.favoritedMovieIds.add(imdbId))
                    handler.onSuccess(_shared.favoritedMovieIds);
            }

            @Override
            public void onFailure(Exception e) { handler.onFailure(e); }
        });
    }

    @Override
    public void removeFavorite(final String imdbId, final ResultHandler<ArrayList<String>> handler) {
        DBServices.shared().removeFavorite(imdbId,new ResultHandler<ArrayList<String>>() {
            @Override
            public void onSuccess(ArrayList<String> data) {
                if (_shared.favoritedMovieIds.remove(imdbId))
                    handler.onSuccess(_shared.favoritedMovieIds);
            }

            @Override
            public void onFailure(Exception e) { handler.onFailure(e); }
        });
    }
}
