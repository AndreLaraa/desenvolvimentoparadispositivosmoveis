package com.example.projetofilmes.Model.Interfaces;

import java.util.ArrayList;

public interface UserMethodsDataBase {
    // métodos que exigem dados externos do usuário
    void create(String email, String password, final ResultHandler<Boolean> handler);
    void login(String email, String password, final ResultHandler<Boolean> handler);
    String getEmail();

    void getFavorites(final ResultHandler< ArrayList<String>> handler);
    void addFavorite(String imdbId, final ResultHandler<ArrayList<String>> handler);
    void removeFavorite(String imdbId, final ResultHandler< ArrayList<String>> handler);
}
