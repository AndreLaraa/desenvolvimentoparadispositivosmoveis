package com.example.projetofilmes;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.projetofilmes.Model.Interfaces.ResultHandler;
import com.example.projetofilmes.Model.User;

import com.google.android.material.snackbar.Snackbar;

public class MainActivity extends AppCompatActivity {
    EditText email;
    EditText senha;
    Button currentUserButton;

    private LoadingDialog loadingDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        email = findViewById(R.id.editTextEmail);
        senha = findViewById(R.id.editTextPassword);
        currentUserButton = findViewById(R.id.buttonLogado);
        loadingDialog = new LoadingDialog(this);
    }

    @Override
    public void onStart() {
        super.onStart();

        updateUI();
    }

    private void updateUI() {

        if (User.shared().isLogged()) {
            currentUserButton.setText("ENTRAR COMO "+User.shared().getEmail());
            currentUserButton.setVisibility(View.VISIBLE);

//            Intent intent = new Intent(MainActivity.this, RecyclerMovies.class );
//            startActivity(intent);
        } else {
            currentUserButton.setVisibility(View.INVISIBLE);
        }
    }

    private void handlingSuccess(){
        loadingDialog.hide();
        Intent intent = new Intent(MainActivity.this, RecyclerMovies.class );
        startActivity(intent);
    }

    public void handlingFailure(Exception e){
        loadingDialog.hide();

        Log.w("firebase problem", ":failure -> ", e);
        View contextView = findViewById(android.R.id.content);

        Snackbar.make(contextView,
                "Falha de autenticaçao: "+e.toString(),
                Snackbar.LENGTH_LONG)
                .show();
    }

    public void entrar(View v) {
        String _email = email.getText().toString();
        String _pass = senha.getText().toString();
        loadingDialog.show();

        User.shared().login(_email, _pass, new ResultHandler<Boolean>() {
            @Override
            public void onSuccess(Boolean data) { handlingSuccess(); }

            @Override
            public void onFailure(Exception e) { handlingFailure(e); }
        });
    }

    public void cadastrar(View v) {
        String _email = email.getText().toString();
        String _pass = senha.getText().toString();
        loadingDialog.show();

        User.shared().create(_email, _pass, new ResultHandler<Boolean>() {
            @Override
            public void onSuccess(Boolean data) { handlingSuccess(); }

            @Override
            public void onFailure(Exception e) { handlingFailure(e); }
        });
    }

    public void entrarComUsuarioAutenticado(View v){
        Intent intent = new Intent(MainActivity.this, RecyclerMovies.class );
        startActivity(intent);
    }
}
