package com.example.projetofilmes;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.Volley;
import com.example.projetofilmes.Model.DataModel;
import com.example.projetofilmes.Model.Interfaces.ResultHandler;
import com.example.projetofilmes.Model.Movie;
import com.google.common.base.Joiner;

public class MovieDetailsActivity extends AppCompatActivity {
    ImageView poster;
    TextView title_detail_movie;
    TextView year;
    TextView textViewInfos1;
    TextView textViewInfos2;
    TextView imdbRating;
    TextView Metascore;
    TextView director;
    TextView plot;
    TextView actors;
    TextView awards;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_details);

        poster = findViewById(R.id.poster);
        title_detail_movie = findViewById(R.id.title_detail_movie);
        year = findViewById(R.id.year);
        textViewInfos1 = findViewById(R.id.textViewInfos1);
        textViewInfos2 = findViewById(R.id.textViewInfos2);
        imdbRating = findViewById(R.id.imdbRating);
        Metascore = findViewById(R.id.Metascore);
        director = findViewById(R.id.director);
        actors = findViewById(R.id.actors);
        awards = findViewById(R.id.awards);
        plot = findViewById(R.id.plot);


        Bundle b = getIntent().getExtras();
        Log.d("AAAAA", "VIEW" + b.getString("imdbId"));

        if(b.containsKey("imdbId")) {
            String aux = b.getString("imdbId");
            DataModel.shared().getMovieByIMDbId(aux, new ResultHandler<Movie>() {
                @Override
                public void onSuccess(final Movie data) {
                    Log.d("AAAAA", "VIEW " + data.Title);
                    MovieDetailsActivity.this.runOnUiThread(new Runnable() {
                        public void run() {
                            Log.d("UI thread", "I am the UI thread");
                            updateUI(data);
                        }
                    });

                }

                @Override
                public void onFailure(Exception e) {
                    finish();
                }
            });

        }
    }

    @Override
    protected void onResume() {
        super.onResume();


    }

    public void updateUI(Movie m){
        Log.d("AAAAA", director.toString());
        Log.d("AAAAA", m.Plot.toString());

        if(m.Title != null)
            title_detail_movie.setText(m.Title);
        if(m.Year != null)
            year.setText(m.Year);
//        R | 162 min | Action, Adventure, Fantasy

        if(m.imdbRating != null)
            imdbRating.setText(m.imdbRating);
        if(m.Metascore != null)
            Metascore.setText(m.Metascore);
        if(m.Director != null)
            director.setText(m.Director);
        if(m.Plot != null)
            plot.setText(m.Plot);
        if(m.Actors != null)
            actors.setText(m.Actors);
        if(m.Awards != null)
            awards.setText(m.Awards);

        String infos1 = "";
        if(m.Rated != null)
            infos1 += m.Rated + "    |    ";

        if(m.Runtime != null)
            infos1 += m.Runtime + "   |   ";

        if(m.genres != null)
            infos1 += m.Genre;

        textViewInfos1.setText(infos1);

        String infos2 = "";
        if(m.Released != null)
            infos2 += m.Released + "    |    ";

        if(m.Country != null)
            infos2 += m.Country + "   -   ";

        if(m.Language != null)
            infos2 += m.Language;

        textViewInfos2.setText(infos2);

        RequestQueue queue = Volley.newRequestQueue(
                this);
        String imageURL = "https://images-na.ssl-images-amazon.com/images/M/MV5BMTY2ODQ3NjMyMl5BMl5BanBnXkFtZTcwODg0MTUzNA@@._V1_SX1777_CR0,0,1777,999_AL_.jpg";


        ImageRequest ir = new ImageRequest(m.Poster, new Response.Listener< Bitmap >() {
            @Override
            public void onResponse(Bitmap response) {
                // callback
//                Singleton.shared().movies.get(0).image = response;
//                recyclerMovies_itemAdapter.notifyDataSetChanged();
                poster.setImageBitmap(response);
            }
        }, 500, 500, null, null);
        queue.add(ir);
        queue.start();


    }
}
