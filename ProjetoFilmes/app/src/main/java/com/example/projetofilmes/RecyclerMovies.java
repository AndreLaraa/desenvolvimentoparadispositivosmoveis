package com.example.projetofilmes;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.util.Pair;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;

import com.example.projetofilmes.Model.DataModel;
import com.example.projetofilmes.Model.Enums.FilterType;
import com.example.projetofilmes.Model.Interfaces.ResultHandler;
import com.example.projetofilmes.Model.User;

import java.util.ArrayList;

public class RecyclerMovies extends AppCompatActivity implements AdapterView.OnItemClickListener {
    RecyclerView recyclerViewMovies;
    RecyclerMovieItem recyclerMovies_item;
    private LoadingDialog loadingDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recycler_movies);

        recyclerViewMovies = findViewById(R.id.recyclerViewFilmes);

        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 2);
        recyclerViewMovies.setLayoutManager(gridLayoutManager);
        loadingDialog = new LoadingDialog(this);

        loadingDialog.show();

        // Get movies and genders
        DataModel.shared().getMoviesByFilter(this, new ResultHandler<Boolean>() {
            @Override
            public void onSuccess(Boolean data) {
                DataModel.shared().getGenders(new ResultHandler<Boolean>() {
                    @Override
                    public void onSuccess(Boolean data) {
                        recyclerMovies_item = new RecyclerMovieItem();
                        recyclerViewMovies.setAdapter(recyclerMovies_item);

                        updateUI();
                    }

                    @Override
                    public void onFailure(Exception e) {}
                });

            }

            @Override
            public void onFailure(Exception e) {}
        });

        User.shared().getFavorites(new ResultHandler<ArrayList<String>>() {
            @Override
            public void onSuccess(ArrayList<String> data) {
                recyclerMovies_item.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Exception e) {
            }
        });
    }

    public void updateUI() {
        loadingDialog.hide();
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        Log.d("AAAAA", "VIEW" + i);

//        Intent intent = new Intent(RecyclerMovies.this, MovieDetailsActivity.class);
//        startActivity(intent);
    }

    // FUNÇÕES DO MENU
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_filter_movies, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.filter_gender) {
            DataModel.shared().setFilterType(FilterType.favorite);
            DataModel.shared().getMapGendersSelected(new ResultHandler<Pair<ArrayList<String>, ArrayList<Boolean>>>() {
                @Override
                public void onSuccess(Pair<ArrayList<String>, ArrayList<Boolean>> data) {
                    showGenderFilters(data);
                    return;
                }

                @Override
                public void onFailure(Exception e) {
                    return;
                }
            });


        } else if (id == R.id.filter_favorites || id == R.id.filter_any) {
            loadingDialog.show();
            DataModel.shared().setFilterType(id == R.id.filter_favorites ? FilterType.favorite : FilterType.any);
            DataModel.shared().getMoviesByFilter(this, new ResultHandler<Boolean>() {
                @Override
                public void onSuccess(Boolean data) {
                    RecyclerMovies.this.runOnUiThread(new Runnable() {
                        public void run() {
                            recyclerMovies_item.notifyDataSetChanged();
                            loadingDialog.hide();
                        }
                    });
                }

                @Override
                public void onFailure(Exception e) {

                }
            });
        }

        return super.onOptionsItemSelected(item);
    }

    private void showGenderFilters(Pair<ArrayList<String>, ArrayList<Boolean>> data) {
        AlertDialog.Builder builder = new AlertDialog.Builder(RecyclerMovies.this);
        builder.setTitle("GÊNEROS");

        final String[] genders = new String[data.first.size()];
        final boolean[] checkedItems = new boolean[data.second.size()];

        for (int i = 0; i < data.second.size(); i++) {
            checkedItems[i] = data.second.get(i);
            genders[i] = data.first.get(i);
        }

        builder.setMultiChoiceItems(genders, checkedItems, new DialogInterface.OnMultiChoiceClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which, boolean isChecked) {

                checkedItems[which] = isChecked;
            }
        });


        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                loadingDialog.show();
                new Thread(new Runnable() {
                    @Override
                    public void run() {

                        ArrayList<Boolean> ret2 = new ArrayList<>();

                        for (int i = 0; i < checkedItems.length; i++) {
                            ret2.add(checkedItems[i]);
                        }

                        DataModel.shared().setGendersToFilter(ret2, new ResultHandler<Boolean>() {
                            @Override
                            public void onSuccess(Boolean data) {
                                DataModel.shared().getMoviesByFilter(RecyclerMovies.this, new ResultHandler<Boolean>() {
                                    @Override
                                    public void onSuccess(Boolean data) {

                                        RecyclerMovies.this.runOnUiThread(new Runnable() {
                                            public void run() {
                                                recyclerMovies_item.notifyDataSetChanged();
                                                loadingDialog.hide();
                                            }
                                        });

                                        return;
                                    }

                                    @Override
                                    public void onFailure(Exception e) {
                                    }
                                });

                            }

                            @Override
                            public void onFailure(Exception e) {
                                loadingDialog.hide();
                            }
                        });
                    }
                }).start();

                return;

            }
        });
        builder.setNegativeButton("Cancel", null);

        AlertDialog dialog = builder.create();
        dialog.show();
    }

}
