package com.example.projetofilmes.Model.Interfaces;

public interface ResultHandler<T> {
    void onSuccess(T data);
    void onFailure(Exception e);
}
