package com.example.projetofilmes.Model;

import android.content.Context;
import android.util.Log;
import android.util.Pair;

import com.example.projetofilmes.Model.Enums.Favorite;
import com.example.projetofilmes.Model.Enums.FilterType;
import com.example.projetofilmes.Model.Interfaces.ResultHandler;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import java.util.List;
import java.util.function.Predicate;
import java.util.function.Supplier;


public class DataModel {
    // Objects used on singleton class
    private List<Movie> movies;
    private List<Movie> movies_with_filter;
    public ArrayList<String> allGenders;

    public FilterType filter;
    public ArrayList<String> genders_filtered;

    // Singleton object and methods
    private static DataModel _shared = new DataModel();

    public static DataModel shared() {
        return _shared;
    }

    private DataModel() {
        movies = new ArrayList<>();
        movies_with_filter = new ArrayList<>();

        filter = FilterType.any;
        genders_filtered = new ArrayList<>();
        allGenders = new ArrayList<>();
    }

    public List<Movie> getMovies() {
        return (_shared.filter == FilterType.any) ? _shared.movies : _shared.movies_with_filter;
    }

    public void getMovieByIMDbId(final String id, final ResultHandler<Movie> handler) {
        Log.d("AAAAA", "VIEW" + id);
        Movie m = _shared.movies.stream()
                .filter(new Predicate<Movie>() {
                    @Override
                    public boolean test(Movie c) {
                        Boolean v = c.imdbID.equals(id);
                        if (v)
                            handler.onSuccess(c);
                        return v;
                    }
                }).findAny().orElse(null);
        if (m == null)
            handler.onFailure(new Exception());
    }

    private void getMoviesOnJSON(Context context, ResultHandler<Boolean> handler) {
        try {

            InputStream stream = context.getAssets().open("filmes.JSON");
            int size = stream.available();
            byte[] buffer = new byte[size];
            stream.read(buffer);
            stream.close();

            String line = new String(buffer, "UTF-8");
            ArrayList<Movie> a = new Gson().fromJson(line, new TypeToken<List<Movie>>() {
            }.getType());

            _shared.movies = a;
            handler.onSuccess(true);
        } catch (IOException e) {

            e.printStackTrace();
            handler.onFailure(e);
        }
    }

    private void returningMoviesByFilter(final ResultHandler<Boolean> handler) {
        _shared.movies_with_filter.clear();

        switch (filter) {
            case any:
                handler.onSuccess(true);
                break;
            case favorite:
                for (Movie m : _shared.movies)
                    if (User.shared().containFavorited(m.imdbID))
                        _shared.movies_with_filter.add(0, m);

                handler.onSuccess(true);
                break;

            case genders:
                int c = 0;
                int size = _shared.movies.size();
                Log.d("AAAAAAAA generos filtrados -->", "" + size + " -- " + _shared.genders_filtered.toString());
                while (c < size) {
                    Movie m = _shared.movies.get(c);
                    if (!Collections.disjoint(m.genres, _shared.genders_filtered)) {
                        Log.d("AAAAAAAA filme -->", m.Title + " - " + m.genres.toString());
                        _shared.movies_with_filter.add(0, m);
                    }

                    c += 1;
                    Log.d("AAAAAAAA dentro do while -->", c + " " + size);

                    if (c == size) {
                        Log.d("AAAAAAAA filmes filtrados -->", "" + _shared.movies_with_filter.toString());
                        handler.onSuccess(true);
                        return;
                    }
                }

                break;
        }
    }

    public void getMoviesByFilter(Context context, final ResultHandler<Boolean> handler) {
        if (_shared.movies.size() == 0) {
            Log.d("AAAAAAAA filmes vazios", "");
            _shared.getMoviesOnJSON(context, new ResultHandler<Boolean>() {
                @Override
                public void onSuccess(Boolean data) {
                    _shared.returningMoviesByFilter(handler);
                }

                @Override
                public void onFailure(Exception e) {
                }
            });
        } else
            _shared.returningMoviesByFilter(handler);

    }

    public void getGenders(ResultHandler<Boolean> handler) {
        if (_shared.allGenders.size() > 0) {
            handler.onSuccess(true);
            return;
        }


        for (Movie m : _shared.movies) {
            String[] genres = m.Genre.split(", ");

            for (String genero_filme : genres) {
                m.genres.add(genero_filme);
                if (!_shared.allGenders.contains(genero_filme))
                    _shared.allGenders.add(genero_filme);
            }

            Collections.sort(_shared.allGenders, new Comparator<String>() {
                @Override
                public int compare(String s1, String s2) {
                    return s1.compareToIgnoreCase(s2);
                }
            });
        }
        handler.onSuccess(true);
        return;

    }

    public void getMapGendersSelected(final ResultHandler<Pair<ArrayList<String>, ArrayList<Boolean>>> handler) {
        final ArrayList<Boolean> gender_selecteds = new ArrayList<>();

        _shared.getGenders(new ResultHandler<Boolean>() {
            @Override
            public void onSuccess(Boolean data) {
                for (int i = 0; i < _shared.allGenders.size(); i++) {
                    gender_selecteds.add(i, _shared.genders_filtered.contains(_shared.allGenders.get(i)));
                }

                Pair<ArrayList<String>, ArrayList<Boolean>> ret = Pair.create(_shared.allGenders, gender_selecteds);
                Log.d("AAAAAAAA getMapGendersSelected  ", "" + ret.second.toString());
                Log.d("AAAAAAAA getMapGendersSelected  ", "" + ret.first.toString());
                handler.onSuccess(ret);
            }

            @Override
            public void onFailure(Exception e) {
                Log.d("AAAAAAAA fodeu", e.toString());
                handler.onFailure(e);
            }
        });

    }

    public void setFilterType(FilterType type) {
        _shared.filter = type;
    }

    public void setGendersToFilter(ArrayList<Boolean> data, ResultHandler<Boolean> handler) {
        setFilterType(FilterType.genders);

        Log.d("AAAAAAAA generos antes de ir DataModel ->", "" + _shared.genders_filtered.toString());
        Log.d("AAAAAAAA generos antes de ir DataModel ->", "" + data.toString());
        _shared.genders_filtered.clear();
        int i = 0;
        for (Boolean _boolean : data) {
            String g = _shared.allGenders.get(i);

            if (_boolean)
                _shared.genders_filtered.add(g);

            i++;
        }
        Log.d("AAAAAAAA generos indo DataModel ->", "" + _shared.genders_filtered.toString());
        handler.onSuccess(true);
        return;

    }

    public void setMovieFavorite(Movie movie, final ResultHandler<Favorite> handler) {
        String imdbID = movie.imdbID;

        if (User.shared().containFavorited(imdbID)) {
            User.shared().removeFavorite(imdbID, new ResultHandler<ArrayList<String>>() {
                @Override
                public void onSuccess(ArrayList<String> data) {
                    handler.onSuccess(Favorite.off);
                }

                @Override
                public void onFailure(Exception e) {
                    handler.onFailure(e);
                }
            });

        } else {

            User.shared().addFavorite(imdbID, new ResultHandler<ArrayList<String>>() {
                @Override
                public void onSuccess(ArrayList<String> data) {
                    handler.onSuccess(Favorite.on);
                }

                @Override
                public void onFailure(Exception e) {
                    handler.onFailure(e);
                }
            });

        }
    }

}