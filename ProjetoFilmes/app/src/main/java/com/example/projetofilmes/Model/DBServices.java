package com.example.projetofilmes.Model;

import android.util.Log;

import androidx.annotation.NonNull;

import com.example.projetofilmes.Model.Interfaces.ResultHandler;
import com.example.projetofilmes.Model.Interfaces.UserMethodsDataBase;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.SetOptions;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

class DBServices implements UserMethodsDataBase {

    private static DBServices _shared = new DBServices();
    private static FirebaseFirestore db;
    private static FirebaseAuth mAuth;
    private static FirebaseUser currentUser;
    private static CollectionReference usersReference;

    private DBServices() {

    }

    public static DBServices shared() {
        db = FirebaseFirestore.getInstance();
        usersReference = db.collection("users");

        mAuth = FirebaseAuth.getInstance();
        currentUser = (mAuth != null) ? mAuth.getCurrentUser() : null;

        return _shared;
    }

    @Override
    public void create(String email, String password, final ResultHandler<Boolean> handler) {

        _shared.mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            _shared.currentUser = _shared.mAuth.getCurrentUser();

                            Map<String, Object> data = new HashMap<>();
                            data.put("uid", _shared.currentUser.getUid());

                            usersReference.document(_shared.currentUser.getEmail()).set(data);

                            handler.onSuccess(true);
                        } else handler.onFailure(task.getException());
                    }
                });
    }

    @Override
    public void login(String email, String password, final ResultHandler<Boolean> handler) {
        _shared.mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            _shared.currentUser = _shared.mAuth.getCurrentUser();
                            handler.onSuccess(true);
                        } else handler.onFailure(task.getException());
                    }
                });

    }

    @Override
    public String getEmail() {
        return _shared.currentUser.getEmail();
    }

    @Override
    public void getFavorites(final ResultHandler<ArrayList<String>> handler) {

        _shared.usersReference.document(_shared.currentUser.getEmail()).get().
                addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                        if (!task.isSuccessful()) {
                            handler.onFailure(task.getException());
                            return;
                        }

                        DocumentSnapshot document = task.getResult();

                        if (document.exists() && document.getData().containsKey("favorites"))
                            handler.onSuccess((ArrayList<String>) document.get("favorites"));
                        else
                            handler.onFailure(task.getException());
                    }
                });
    }

    @Override
    public void addFavorite(String imdbId, final ResultHandler<ArrayList<String>> handler) {
        final ArrayList<String> aux = (ArrayList<String>) User.shared().favoritedMovieIds.clone();
        aux.add(imdbId);

        Map<String, Object> docData = new HashMap<>();
        docData.put("favorites", aux);

        _shared.usersReference.document(_shared.currentUser.getEmail()).set(docData, SetOptions.merge())
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        Date a = new Date();
                        Log.d("ABLUBLÉBABLU addFavorite", ""+a.toString());
                        if (!task.isSuccessful())
                            handler.onFailure(task.getException());
                        else
                            handler.onSuccess(aux);
                    }
                });
    }

    @Override
    public void removeFavorite(String imdbId, final ResultHandler<ArrayList<String>> handler) {
        final ArrayList<String> aux = (ArrayList<String>) User.shared().favoritedMovieIds.clone();
        aux.remove(imdbId);

        Map<String, Object> docData = new HashMap<>();
        docData.put("favorites", aux);

        _shared.usersReference.document(_shared.currentUser.getEmail()).set(docData, SetOptions.merge())
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (!task.isSuccessful())
                            handler.onFailure(task.getException());
                        else
                            handler.onSuccess(aux);
                    }
                });
    }
}
